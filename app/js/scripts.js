$( document ).ready(function() {

	$('#price__test__phone').inputmask({ "mask": "+7 (999) 999-99-99" });


	/*Определяем переменные в которые будем вводить данные для показа результата*/
	var finalHeader = $('.price__test__final__result .price__test__header');
	var finalFirstList = $('.price__test__final__conditions__list');
	var finalSecondList = $('.price__final__additional__list');
	var finalPreResult = $('.pre__result__text');
	var finalBlock = $('.price__test__final__results__block');
	var finalBlockItem = $('.price__test__final__results__item');
	var finalItemText = $('.price__test__final__results__item__text');
	var finalPrice = $('.price__test__final__results__item__price');

	/*По клику очищаем все инпуты формы, скрываем текущий экран вопроса и кидаем на стартовый*/

	$('.change__service').click(function() {
		location.reload()
		// $(this).parents('form').trigger("reset");
		// $(this).parent('.question, .phone__enter__block, .price__test__final__result').addClass('animated fadeOutUp').delay(300).hide();
		// $('.price__start__test__screen').delay(400).show().removeClass('fadeOutUp').addClass("zoomIn").children().removeClass('fadeOutUp').addClass("zoomIn");
	});

	/*Проверяем чекнут ли инпут и разблокируем кнопку для перехода на следующий вопрос*/
	$('.price__test__input').on('change', function() {
		if ($(this).is(':checked')) {
			console.log('input checked');
			$(this).parent('.prices__test__inputs__wrap').next('.price__test__next__screen').prop('disabled', false);
		} else {
			console.log('input not checked');
			$(this).parent('.prices__test__inputs__wrap').next('.price__test__next__screen').prop('disabled', true);
		}
	});

	var inputAnswer = '0';
	$('.price__test__next__screen').click(function(nextStep) {
		nextStep.preventDefault(nextStep);
		$(this).addClass('animated fadeOutUp');
		$(this).siblings().addClass('animated fadeOutUp');
		$(this).parent().addClass('animated fadeOutUp').delay(300).hide();

		/*Проверка на класс, если есть, то всплывёт форма набора телефона*/

		if ($(this).hasClass('last__screen__click')) {
			$('.phone__enter__block').delay(400).show().children().addClass("animated zoomIn");
			console.log(inputAnswer);
		}
		
		inputAnswer = inputAnswer + '-' + $(this).prev('.prices__test__inputs__wrap').find('input:checked').data('answer');
		console.log(inputAnswer);

		/*Проверка дата атрибута и переход на соответствующий экран линии вопроса*/

		
		switch (inputAnswer) {
			/*Линия первого вопроса*/
			case '0-1':
			$('.question[data-step="1"]').delay(400).show();
			$('.question[data-step="1"]').children().addClass("animated zoomIn");
			break;

			case '0-1-1':
			case '0-1-2':
			case '0-1-3':
			case '0-1-4':
			case '0-1-5':
			case '0-1-6':
			$('.question[data-step="1-2"]').delay(400).show();
			$('.question[data-step="1-2"]').children().addClass("animated zoomIn");
			break;

			case '0-1-1-1':
			case '0-1-2-1':
			case '0-1-3-1':
			case '0-1-4-1':
			case '0-1-5-1':
			case '0-1-6-1':
			case '0-1-1-2':
			case '0-1-2-2':
			case '0-1-3-2':
			case '0-1-4-2':
			case '0-1-5-2':
			case '0-1-6-2':
			case '0-1-1-3':
			case '0-1-2-3':
			case '0-1-3-3':
			case '0-1-4-3':
			case '0-1-5-3':
			case '0-1-6-3':
			$('.question[data-step="1-2-3"]').delay(400).show();
			$('.question[data-step="1-2-3"]').children().addClass("animated zoomIn");
			break;

			/*Линия первого вопроса закончилась*/

			/*Линия второго вопроса*/

			case '0-2':
			$('.question[data-step="2"]').delay(400).show().children().addClass("animated zoomIn");
			break;

			/*Линия второго вопроса закончилась*/

			/*Линия третьего вопроса*/
			case '0-3':
			$('.question[data-step="3"]').delay(400).show().children().addClass("animated zoomIn");
			break;

			case '0-3-1':
			case '0-3-2':
			$('.question[data-step="3-1"]').delay(400).show().children().addClass("animated zoomIn");
			break;

			/*Линия третьего вопроса закончилась*/

			/*Линия четвертого вопроса*/
			
			case '0-4':
			$('.question[data-step="4"]').delay(400).show().children().addClass("animated zoomIn");
			break;

			case '0-4-1':
			case '0-4-2':
			case '0-4-3':
			case '0-4-4':
			$('.question[data-step="4-1"]').delay(400).show().children().addClass("animated zoomIn");
			break;

			/*Линия четвертого вопроса закончилась*/

			/*Линия пятого вопроса*/
			case '0-5':
			$('.question[data-step="5"]').delay(400).show().children().addClass("animated zoomIn");
			break;

			case '0-5-1':
			$('.phone__enter__block').find('.phone__enter__descr').html('И получите приветственный бонус <span>1000 рублей</span>.');
			$('.question[data-step="5-1"]').delay(400).show().children().addClass("animated zoomIn");
			break;

			case '0-5-2':
			$('.question[data-step="5-1"]').delay(400).show().children().addClass("animated zoomIn");
			$('.phone__enter__block').find('.phone__enter__descr').hide();
			break;
			/*Линия пятого вопроса закончилась*/

			/*Линия шестого вопроса*/
			case '0-6':
			$('.question[data-step="6"]').delay(400).show().children().addClass("animated zoomIn");
			break;

			case '0-6-1':
			$('.phone__enter__block').find('.phone__enter__descr').html('И получите приветственный бонус <span>1000 рублей</span>.');
			$('.question[data-step="6-1"]').delay(400).show().children().addClass("animated zoomIn");
			break;

			case '0-6-2':
			$('.phone__enter__block').find('.phone__enter__descr').html('И получите скидку <span>12%</span> на лечение.');
			$('.question[data-step="6-1"]').delay(400).show().children().addClass("animated zoomIn");
			break;
			/*Линия шестого вопроса закончилась*/

			/*Линия седьмого вопроса*/
			case '0-7':
			$('.question[data-step="7"]').delay(400).show().children().addClass("animated zoomIn");
			break;

			case '0-7-1':
			case '0-7-2':
			case '0-7-3':
			$('.question[data-step="7-1"]').delay(400).show().children().addClass("animated zoomIn");
			break;
			/*Линия седьмого вопроса закончилась*/

			/*Линия восьмого вопроса*/
			case '0-8':
			$('.question[data-step="8"]').delay(400).show().children().addClass("animated zoomIn");
			break;

			case '0-8-1':
			$('.phone__enter__header').html('Введите Ваш номер телефона');
			$('.phone__enter__block').find('.phone__enter__descr').html('и получите расчет стоимости на профессиональную чистку зубов с фторированием.');
			break;

			case '0-8-2':
			$('.phone__enter__header').html('Введите Ваш номер телефона');
			$('.phone__enter__block').find('.phone__enter__descr').html('и получите расчет стоимости на профессиональную трехэтапную чистку зубов.');
			break;
			/*Линия восьмого вопроса закончилась*/

			/*Линия девятого вопроса*/
			case '0-9':
			$('.question[data-step="9"]').delay(400).show().children().addClass("animated zoomIn");
			break;

			case '0-9-1':
			case '0-9-2':
			$('.question[data-step="9-1"]').delay(400).show().children().addClass("animated zoomIn");
			$('.phone__enter__header').html('Введите Ваш номер телефона');
			$('.phone__enter__block').find('.phone__enter__descr').html('и получите расчет стоимости и сертификат на <span>12000</span> рублей на протезирование.');
			break;
			/*Линия девятого вопроса закончилась*/

			default:
    	nextStep.stopImmediatePropagation();
		}
	});



	$('.price__test__get__offer').click(function(e) {
		e.preventDefault(e);

		/*Генерация контента в зависимости от созданной цепочки дата атрибутов в линии вопроса*/

		/*Линия первого вопроса*/
		switch (inputAnswer) {
			case '0-1-1-1-1':
			case '0-1-2-1-1':
			case '0-1-3-1-1':
			case '0-1-4-1-1':
			case '0-1-5-1-1':
			case '0-1-6-1-1':
			case '0-1-1-2-1':
			case '0-1-2-2-1':
			case '0-1-3-2-1':
			case '0-1-4-2-1':
			case '0-1-5-2-1':
			case '0-1-6-2-1':
			case '0-1-1-3-1':
			case '0-1-2-3-1':
			case '0-1-3-3-1':
			case '0-1-4-3-1':
			case '0-1-5-3-1':
			case '0-1-6-3-1':
			finalHeader.html('Спасибо! На Ваш номер начислены <span>12 000 рублей</span> в виде бонусов.');
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена без скидок:');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 17 900 руб.');
			$('.price__test__final__results__item.second').find(finalItemText).html('Цена с учетом сертификата на 12000 рублей (можно оплатить до 12% от стоимости лечения)');
			$('.price__test__final__results__item.second').find(finalPrice).html('от 15 752 руб.');
			$('.price__test__final__results__item.third').find(finalItemText).html('Цена с учетом возрата 13% НДФЛ (поможем с этим)');
			$('.price__test__final__results__item.third').find(finalPrice).html('от 13 704,24 руб.');
			break;

			case '0-1-1-1-2':
			case '0-1-2-1-2':
			case '0-1-3-1-2':
			case '0-1-4-1-2':
			case '0-1-5-1-2':
			case '0-1-6-1-2':
			case '0-1-1-2-2':
			case '0-1-2-2-2':
			case '0-1-3-2-2':
			case '0-1-4-2-2':
			case '0-1-5-2-2':
			case '0-1-6-2-2':
			case '0-1-1-3-2':
			case '0-1-2-3-2':
			case '0-1-3-3-2':
			case '0-1-4-3-2':
			case '0-1-5-3-2':
			case '0-1-6-3-2':
			finalHeader.html('Спасибо! На Ваш номер начислены <span>12 000 рублей</span> в виде бонусов.');
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена без скидок:');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 20 900 руб.');
			$('.price__test__final__results__item.second').find(finalItemText).html('Цена с учетом сертификата на 12000 рублей (можно оплатить до 12% от стоимости лечения)');
			$('.price__test__final__results__item.second').find(finalPrice).html('от 18 032 руб.');
			$('.price__test__final__results__item.third').find(finalItemText).html('Цена с учетом возрата 13% НДФЛ (поможем с этим)');
			$('.price__test__final__results__item.third').find(finalPrice).html('от 15 687 руб.');
			break;

			case '0-1-1-1-3':
			case '0-1-2-1-3':
			case '0-1-3-1-3':
			case '0-1-4-1-3':
			case '0-1-5-1-3':
			case '0-1-6-1-3':
			case '0-1-1-2-3':
			case '0-1-2-2-3':
			case '0-1-3-2-3':
			case '0-1-4-2-3':
			case '0-1-5-2-3':
			case '0-1-6-2-3':
			case '0-1-1-3-3':
			case '0-1-2-3-3':
			case '0-1-3-3-3':
			case '0-1-4-3-3':
			case '0-1-5-3-3':
			case '0-1-6-3-3':
			finalHeader.html('Спасибо! На Ваш номер начислены <span>12 000 рублей</span> в виде бонусов.');
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена без скидок:');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 27 900 руб.');
			$('.price__test__final__results__item.second').find(finalItemText).html('Цена с учетом сертификата на 12000 рублей (можно оплатить до 12% от стоимости лечения)');
			$('.price__test__final__results__item.second').find(finalPrice).html('от 24 552 руб.');
			$('.price__test__final__results__item.third').find(finalItemText).html('Цена с учетом возрата 13% НДФЛ (поможем с этим)');
			$('.price__test__final__results__item.third').find(finalPrice).html('от 21 360 руб.');
			break;

			case '0-1-1-1-4':
			case '0-1-2-1-4':
			case '0-1-3-1-4':
			case '0-1-4-1-4':
			case '0-1-5-1-4':
			case '0-1-6-1-4':
			case '0-1-1-2-4':
			case '0-1-2-2-4':
			case '0-1-3-2-4':
			case '0-1-4-2-4':
			case '0-1-5-2-4':
			case '0-1-6-2-4':
			case '0-1-1-3-4':
			case '0-1-2-3-4':
			case '0-1-3-3-4':
			case '0-1-4-3-4':
			case '0-1-5-3-4':
			case '0-1-6-3-4':
			finalHeader.html('Спасибо! На Ваш номер начислены <span>12 000 рублей</span> в виде бонусов.');
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена без скидок:');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 32 900 руб.');
			$('.price__test__final__results__item.second').find(finalItemText).html('Цена с учетом сертификата на 12000 рублей (можно оплатить до 12% от стоимости лечения)');
			$('.price__test__final__results__item.second').find(finalPrice).html('от 28 952 руб.');
			$('.price__test__final__results__item.third').find(finalItemText).html('Цена с учетом возрата 13% НДФЛ (поможем с этим)');
			$('.price__test__final__results__item.third').find(finalPrice).html('от 25 188 руб.');
			break;

			/*Линия первого вопроса закончилась*/

			/*Линия второго вопроса*/

			case '0-2-1':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена:');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 5000 руб.');
			break;

			case '0-2-2':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена:');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 4250 руб.');
			break;

			case '0-2-3':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена:');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 2500 руб.');
			break;

			case '0-2-4':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена:');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 1900 руб.');
			break;

			case '0-2-5':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена:');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 1850 руб.');
			break;
			/*Линия второго вопроса закончилась*/

			/*Линия третьего вопроса*/
			case '0-3-1-1':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first, .second').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена без скидки:');
			$('.price__test__final__results__item.first').find(finalPrice).html('300 руб.');
			$('.price__test__final__results__item.second').find(finalItemText).html('Цена со скидкой');
			$('.price__test__final__results__item.second').find(finalPrice).html('240 руб.');
			break;

			case '0-3-1-2':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first, .second').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена без скидки:');
			$('.price__test__final__results__item.first').find(finalPrice).html('750 руб.');
			$('.price__test__final__results__item.second').find(finalItemText).html('Цена со скидкой');
			$('.price__test__final__results__item.second').find(finalPrice).html('600 руб.');
			break;

			case '0-3-1-3':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first, .second').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена без скидки:');
			$('.price__test__final__results__item.first').find(finalPrice).html('2500 руб.');
			$('.price__test__final__results__item.second').find(finalItemText).html('Цена со скидкой');
			$('.price__test__final__results__item.second').find(finalPrice).html('2000 руб.');
			break;

			case '0-3-1-4':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first, .second').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена без скидки:');
			$('.price__test__final__results__item.first').find(finalPrice).html('950 руб.');
			$('.price__test__final__results__item.second').find(finalItemText).html('Цена со скидкой');
			$('.price__test__final__results__item.second').find(finalPrice).html('760 руб.');
			break;

			case '0-3-2-1':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена:');
			$('.price__test__final__results__item.first').find(finalPrice).html('300 руб.');
			break;

			case '0-3-2-2':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена:');
			$('.price__test__final__results__item.first').find(finalPrice).html('750 руб.');
			break;

			case '0-3-2-3':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена:');
			$('.price__test__final__results__item.first').find(finalPrice).html('2500 руб.');
			break;

			case '0-3-2-4':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена:');
			$('.price__test__final__results__item.first').find(finalPrice).html('950 руб.');
			break;

			/*Линия третьего вопроса закончилась*/

			/*Линия четвертого вопроса*/

			case '0-4-1-1':
			case '0-4-1-2':
			case '0-4-1-3':
			case '0-4-1-4':
			finalHeader.html('Спасибо! На Ваш номер начислены <span>12 000 рублей</span> в виде бонусов.');
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена без скидок:');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 9900 руб.');
			$('.price__test__final__results__item.second').find(finalItemText).html('Цена с учетом сертификата на 12000 рублей (можно оплатить до 12% от стоимости лечения)');
			$('.price__test__final__results__item.second').find(finalPrice).html('от 8712 руб.');
			$('.price__test__final__results__item.third').find(finalItemText).html('Цена с учетом возрата 13 % НДФЛ (поможем с этим)');
			$('.price__test__final__results__item.third').find(finalPrice).html('от 7579 руб.');
			break;

			case '0-4-2-1':
			case '0-4-2-2':
			case '0-4-2-3':
			case '0-4-2-4':
			finalHeader.html('Спасибо! На Ваш номер начислены <span>12 000 рублей</span> в виде бонусов.');
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена без скидок:');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 14 900 руб.');
			$('.price__test__final__results__item.second').find(finalItemText).html('Цена с учетом сертификата на 12000 рублей (можно оплатить до 12% от стоимости лечения)');
			$('.price__test__final__results__item.second').find(finalPrice).html('от 13 112 руб.');
			$('.price__test__final__results__item.third').find(finalItemText).html('Цена с учетом возрата 13 % НДФЛ (поможем с этим)');
			$('.price__test__final__results__item.third').find(finalPrice).html('от 11 407 руб.');
			break;

			case '0-4-3-1':
			case '0-4-3-2':
			case '0-4-3-3':
			case '0-4-3-4':
			finalHeader.html('Спасибо! На Ваш номер начислены <span>12 000 рублей</span> в виде бонусов.');
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена без скидок:');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 75 000 руб.');
			$('.price__test__final__results__item.second').find(finalItemText).html('Цена с учетом сертификата на 12000 рублей (можно оплатить до 12% от стоимости лечения)');
			$('.price__test__final__results__item.second').find(finalPrice).html('от 66 000 руб.');
			$('.price__test__final__results__item.third').find(finalItemText).html('Цена с учетом возрата 13 % НДФЛ (поможем с этим)');
			$('.price__test__final__results__item.third').find(finalPrice).html('от 57 420 руб.');
			break;

			case '0-4-4-1':
			case '0-4-4-2':
			case '0-4-4-3':
			case '0-4-4-4':
			finalHeader.html('Спасибо! На Ваш номер начислены <span>12 000 рублей</span> в виде бонусов.');
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена без скидок:');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 149 000 руб.');
			$('.price__test__final__results__item.second').find(finalItemText).html('Цена с учетом сертификата на 12000 рублей (можно оплатить до 12% от стоимости лечения)');
			$('.price__test__final__results__item.second').find(finalPrice).html('от 131 120 руб.');
			$('.price__test__final__results__item.third').find(finalItemText).html('Цена с учетом возрата 13 % НДФЛ (поможем с этим)');
			$('.price__test__final__results__item.third').find(finalPrice).html('от 114 074 руб.');
			break;

			/*Линия четвертого вопроса закончилась*/

			/*Линия пятого вопроса*/
			case '0-5-1-1':
			case '0-5-1-2':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе Ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first, .second').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Отбеливание зубов Opalesence:');
			$('.price__test__final__results__item.first').find(finalPrice).html('8900 руб.');
			$('.price__test__final__results__item.second').find(finalItemText).html('Отбеливание зубов Zoom 4:');
			$('.price__test__final__results__item.second').find(finalPrice).html('20 900 рублей.');
			break;

			case '0-5-2-1':
			case '0-5-2-2':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе Ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first, .second').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Отбеливание зубов Opalesence:');
			$('.price__test__final__results__item.first').find(finalPrice).html('9900 руб.');
			$('.price__test__final__results__item.second').find(finalItemText).html('Отбеливание зубов Zoom 4:');
			$('.price__test__final__results__item.second').find(finalPrice).html('21 900 рублей.');
			break;
			/*Линия пятого вопроса закончилась*/

			/*Линия шестого вопроса*/
			case '0-6-1-1':
			case '0-6-2-1':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе Ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).not('.first').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Лечение кариеса');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 3200 рублей');
			finalSecondList.find('.price__final__additional__list__item:last-of-type').hide();
			break;

			case '0-6-1-2':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе Ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).hide();
			break;

			case '0-6-2-2':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе Ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			$(finalBlockItem).hide();
			finalSecondList.find('.price__final__additional__list__item:last-of-type').hide();
			break;
			/*Линия шестого вопроса закончилась*/

			/*Линия седьмого вопроса*/
			case '0-7-1-1':
			case '0-7-1-2':
			case '0-7-1-3':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе Ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			finalSecondList.hide();
			$(finalBlockItem).not('.first').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Реставрация зуба');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 5300 рублей.');
			break;

			case '0-7-2-1':
			case '0-7-2-2':
			case '0-7-2-3':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе Ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			finalSecondList.hide();
			$(finalBlockItem).not('.first, .second').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Реставрация зуба');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 5300 рублей.');
			$('.price__test__final__results__item.second').find(finalItemText).html('Восстановление зуба под коронку');
			$('.price__test__final__results__item.second').find(finalPrice).html('от 10 900 рублей.');
			break;

			case '0-7-3-1':
			case '0-7-3-2':
			case '0-7-3-3':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе Ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			finalSecondList.hide();
			$(finalBlockItem).not('.first').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Восстановление зуба под коронку');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 10 900 рублей.');
			break;
			/*Линия седьмого вопроса закончилась*/

			/*Линия восьмого вопроса*/
			case '0-8-1':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе Ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			finalSecondList.hide();
			$(finalBlockItem).not('.first').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Чистка зубов с гарантией результата и укреплением (фторированием в каппах):');
			$('.price__test__final__results__item.first').find(finalPrice).html('3700 рублей.');
			break;

			case '0-8-2':
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе Ваших ответов:');
			finalFirstList.hide();
			finalPreResult.hide();
			finalSecondList.hide();
			$(finalBlockItem).not('.first').hide();
			$('.price__test__final__results__item.first').find(finalItemText).html('Чистка зубов с гарантией результата:');
			$('.price__test__final__results__item.first').find(finalPrice).html('2900 рублей.');
			break;
			/*Линия восьмого вопроса закончилась*/

			/*Линия девятого вопроса*/
			case '0-9-1-1':
			case '0-9-1-2':
			case '0-9-1-3':
			finalFirstList.hide();
			finalPreResult.hide();
			finalSecondList.hide();
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе Ваших ответов:');
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена без скидок:');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 27 390 рублей за одну челюсть');
			$('.price__test__final__results__item.second').find(finalItemText).html('Цена с учетом сертификата на 12000 рублей (можно оплатить до 12% от стоимости лечения)');
			$('.price__test__final__results__item.second').find(finalPrice).html('от 24 552 руб.');
			$('.price__test__final__results__item.third').find(finalItemText).html('Цена с учетом возрата 13 % НДФЛ (поможем с этим)');
			$('.price__test__final__results__item.third').find(finalPrice).html('от 21 360 руб.');
			break;

			case '0-9-2-1':
			case '0-9-2-2':
			case '0-9-2-3':
			finalFirstList.hide();
			finalPreResult.hide();
			finalSecondList.hide();
			finalHeader.html('Спасибо! Предварительный расчет стоимости на основе Ваших ответов:');
			$('.price__test__final__results__item.first').find(finalItemText).html('Цена без скидок:');
			$('.price__test__final__results__item.first').find(finalPrice).html('от 167 000 рублей за одну челюсть');
			$('.price__test__final__results__item.second').find(finalItemText).html('Цена с учетом сертификата на 12000 рублей (можно оплатить до 12% от стоимости лечения)');
			$('.price__test__final__results__item.second').find(finalPrice).html('от 146 960 руб.');
			$('.price__test__final__results__item.third').find(finalItemText).html('Цена с учетом возрата 13 % НДФЛ (поможем с этим)');
			$('.price__test__final__results__item.third').find(finalPrice).html('от 127 885 руб.');
			break;
			/*Линия девятого вопроса закончилась*/
		}

		$(this).addClass('animated fadeOutUp');
		$(this).siblings().addClass('animated fadeOutUp');
		$(this).parent().addClass('animated fadeOutUp').delay(300).hide();
		$('.price__test__final__result').delay(400).show().children().addClass("animated zoomIn");

	});
});