var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    mozjpeg = require('imagemin-mozjpeg'),
    svgo = require('imagemin-svgo'),
    minifyjs = require('gulp-uglify'),
    rigger = require('gulp-rigger'),
    pngquant = require('imagemin-pngquant'),
    concat = require('gulp-concat-css'),
    rename = require('gulp-rename');

gulp.task('sass', function(){ // Создаем таск Sass
    return gulp.src('app/css/*.scss')
    .pipe(sass())
    .pipe(autoprefixer(['last 5 versions', '> 1%', 'ie 10'], { cascade: true }))
    .pipe(concat('bundle'))
    .pipe(minifycss())
    .pipe(rename({suffix:'.min',extname:'.css'}))
    .pipe(gulp.dest('dist/css'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('html', function() {
    gulp.src('app/*.html')
    .pipe(rigger())
    .pipe(gulp.dest('dist/'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('minjs', function() {
    gulp.src('app/js/libs.js')
    .pipe(rigger())
    .pipe(rename({basename:'bundle',suffix:'.min',extname:'.js'}))
    .pipe(minifyjs())
    .pipe(gulp.dest('dist/js/'))
    .pipe(browserSync.reload({stream: true}));
});


gulp.task('imagemin', function() {
    gulp.src('app/img/*')
    .pipe(imagemin([
        mozjpeg({progressive: true, quality: 100}),
        pngquant({quality: 100}),
        svgo()
    ]))
    .pipe(gulp.dest('dist/img/'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('fonts', function() {
    gulp.src('app/fonts/**/*.*')
    .pipe(gulp.dest('dist/fonts/'))
});

gulp.task('browser-sync', function() { // Создаем таск browser-sync
    browserSync({
        server: {
            baseDir: "dist/"
        },
        notify: false
    });
});

gulp.task('watch', ['browser-sync', 'sass','html','imagemin'], function() {
    gulp.watch('app/css/*.scss', ['sass']),
    gulp.watch('app/**/*.html', ['html']),
    gulp.watch('app/img/*', ['imagemin']),
    gulp.watch('app/js/*.js', ['minjs']);
});